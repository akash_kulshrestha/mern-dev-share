const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
const db = require("./app/models");
UserController = require("./app/controllers/user.controller");
db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log("Connected to the database!");
    })
    .catch(err => {
        console.log("Cannot connect to the database!", err);
        process.exit();
    });

var corsOptions = {
    origin: "http://localhost:3000"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
    res.json({ message: "Welcome to Users application." });
});


app.post("/api/users",async (req, res) => {
    let body = req.body;
    let data =await  UserController.create(body);
    res.json({ data: body, message: "created successfully" });
});

app.get("/api/users/:id?",async (req, res) => {
    let data=null;
    req.params.id ?
    data = await UserController.findOne(req.params.id)
    :
    data = await UserController.findAll(req,res);
    res.json({ data: data });
});

app.put("/api/users/:id",async (req, res) => {
    data = await UserController.update(req.params.id,req.body);
    res.json({ data: data, message: "Updated successfully" });
});

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});