const db = require("../models");
const user = db.users;

// Create and Save a new user
exports.create = async (body) => {
  var myData = new user(body);
  return myData.save();
};

// Retrieve all users from the database.
exports.findAll = async (req,res) => {
  let condition={}
  let alldata=[];
  let data=  await user.find(condition);
  return data;
};

// Find a single user with an id
exports.findOne = async (id) => {
  let data=  await user.findOne({_id:id});
  return data;
};
// Update a user by the id in the request
exports.update = async (id, body) => {
  let data=  await user.update({_id:id},body);
  //let data=  await user.findOne({_id:id});
  return data;
};

// Delete a user with the specified id in the request
exports.delete = (req, res) => {

};

// Delete all users from the database.
exports.deleteAll = (req, res) => {

};

// Find all published users
exports.findAllPublished = (req, res) => {

};