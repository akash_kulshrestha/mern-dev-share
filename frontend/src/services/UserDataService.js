import http from "../http-common";

class UserDataService {
  getAll= async() =>{
    let data= await http.get("/users");
    return data.data.data
  }

  get = async (id)=> {
    let data=  await http.get(`/users/${id}`);
    return data.data.data
  }

  create= async (data) =>{
    return await http.post("/users", data);
  }

  update= async (id, data) =>{
    return await http.put(`/users/${id}`, data);
  }

}

export default new UserDataService();