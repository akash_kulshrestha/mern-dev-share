import React, { Component } from "react";
import { Switch, Route, Redirect, Link } from 'react-router-dom'

import "bootstrap/dist/css/bootstrap.min.css";
import UserListPage from "./components/UserListPage";
import AddUser from "./components/AddUser;";

class App extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <Link to={"/"} className="nav-link">
            Develope by Akash
            </Link>

          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/users"} className="nav-link">
                Users
              </Link>
            </li>
          </div>
        </nav>

        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/users"]} component={UserListPage} />
            <Route exact path="/add" component={AddUser} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
