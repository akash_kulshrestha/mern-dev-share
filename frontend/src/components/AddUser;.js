import React from 'react';
import UserDataService from '../services/UserDataService';
import $ from 'jquery';

class AddUser extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
        }
    }

    componentDidMount() {
        this.setData();
    }

    componentDidUpdate = (prevProps) => {
        if (prevProps.date !== this.props.date) {
            this.setState(this.setData())
        }
    }

    setData = async () => {
        let data = this.props.data;
        this.setState({ data })
    }

    onSave = async () => {
        let res = null;
        console.log(this.state.data);
        if (this.state.data.id) {
            res = await UserDataService.update(this.state.data.id, this.state.data)
        } else {
            res = await UserDataService.create(this.state.data);
        }
        if (res) {
            this.props.setData();
            $(".close").trigger('click');
        }
    }

    onChange = (e) => {
        let data = { ...this.state.data }
        data[e.target.name] = e.target.value;
        console.log(data);
        this.setState({
            data
        })
    }

    render() {
        return (
            <div className="modal-content">
                <div className="modal-header">
                    <h4 className="modal-title pl-0">
                        {
                            this.state.data.id?
                            <>Update User</>
                            :<>Add New User</>
                            
                        }
                    </h4>
                    <button className="btn-ivdp-primary btn-theme" onClick={() => this.onSave()}>Save</button>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body">
                    <div className="row">
                        <div className="col-md-2">
                            <label className="mr-2">First Name:</label>
                        </div>
                        <div className="col-md-4">
                            <input type="text"
                                onChange={this.onChange}
                                value={this.state.data.firstName || ""}
                                name="firstName" placeholder="Enter first name..." />
                        </div>
                        <div className="col-md-2">
                            <label className="mr-2">Last Name:</label>
                        </div>
                        <div className="col-md-4">
                            <input type="text" name="lastName"
                                onChange={this.onChange}
                                value={this.state.data.lastName || ""}
                                placeholder="Enter last name..." />
                        </div>

                        <div className="col-md-12">
                            <div className="row">
                                <div className="col-md-2">
                                    <label className="mr-2">Email:</label>
                                </div>
                                <div className="col-md-4">
                                    <input type="text" name="email"
                                        onChange={this.onChange}
                                        value={this.state.data.email || ""}
                                        placeholder="Enter email..." />

                                </div>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="row">
                                <div className="col-md-2">
                                    <label className="mr-2">Role:</label>
                                </div>
                                <div className="col-md-4">
                                    <select
                                        name="role"
                                        onChange={this.onChange}
                                        value={this.state.data.role || ""}
                                    >
                                        <option value="" ></option>
                                        <option value="Admin" >Admin</option>
                                        <option value="Partner" >Partner</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="row">
                                <div className="col-md-2">
                                    <label className="mr-2">status:</label>
                                </div>
                                <div className="col-md-4">
                                    <select
                                        name="status"
                                        onChange={this.onChange}
                                        value={this.state.data.status || ""}
                                    >
                                        <option value="" ></option>
                                        <option value="Active" >Active</option>
                                        <option value="InActive" >InActive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AddUser;