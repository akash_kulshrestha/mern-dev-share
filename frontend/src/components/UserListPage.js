import React from 'react';
import UserDataService from '../services/UserDataService';
import { MDBDataTable, MDBBtn } from 'mdbreact';
import AddUser from './AddUser;';

const completedata = {
    columns: [
        {
            label: 'First Name',
            field: 'firstName',
            sort: 'asc',
            width: 150
        },
        {
            label: 'Last Name',
            field: 'lastName',
            sort: 'asc',
            width: 270
        },
        {
            label: 'Role',
            field: 'role',
            sort: 'asc',
            width: 200
        },
        {
            label: 'Email',
            field: 'email',
            sort: 'asc',
            width: 100
        },
        {
            label: 'Status',
            field: 'status',
            sort: 'asc',
            width: 50
        }, {
            label: 'Action',
            field: 'action',
            sort: 'asc',
            width: 100
        },
    ],
    rows: []
};


class UserListPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            datatable: null,
            data: [],
            form: null
        }
    }

    componentDidMount() {
        this.setData();
    }

    componentDidUpdate = (prevProps) => {
        if (prevProps.time !== this.props.time) {
            this.setState(this.setData())
        }
    }

    setData = async () => {
        // let id = "5feaa4f772cc332a22309874";
        // let oneRecord = await UserDataService.get(id);
        // let request = {
        //     firstName: "manoj",
        //     lastName: "kumar"
        // }
        //  oneRecord = await UserDataService.update(id ,request);
        // let inserted = await UserDataService.create(request);
        let data = await UserDataService.getAll();
        if (data) {
            this.setState({
                data: data
            }, () => this.createTable())
        }
    }


    createTable = () => {
        let tabledata = completedata;
        tabledata.rows = [...this.state.data];
        tabledata.rows.map((_r) => {
            _r.action = (<MDBBtn data-toggle="modal" data-target="#formModel" style={{ backgroundColor: "blue", color: "#fff" }} size="sm" onClick={(id) => this.editClick(_r.id)}>edit</MDBBtn>)
        });
        let datatable = (
            <MDBDataTable
                striped
                bordered
                small
                data={tabledata}
            />
        )
        this.setState({ datatable })
    }

    editClick = async (id = "") => {
        let data = {}
        if (id) {
            data = await UserDataService.get(id);
        }
        let form = (<AddUser data={data} setData={()=>this.setData()} date={new Date()}/>)
        this.setState({ form })
    }

    render() {
        return (
            <div className="dashboard">
                <h1>Users</h1>
                <button className="btn-ivdp-primary btn-theme" data-toggle="modal" data-target="#formModel" onClick={() => this.editClick()}>Add New</button>
                {
                    this.state.datatable
                }
                <div className="modal left-popup" id="formModel">
                    <div className="modal-dialog modal-lg">
                        {
                            this.state.form
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default UserListPage;