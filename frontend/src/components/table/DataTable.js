import React from 'react';
import { MDBDataTable,MDBBtn } from 'mdbreact';
const data = {
    columns: [
        {
            label: 'First Name',
            field: 'firstName',
            sort: 'asc',
            width: 150
        },
        {
            label: 'Last Name',
            field: 'lastName',
            sort: 'asc',
            width: 270
        },
        {
            label: 'Role',
            field: 'role',
            sort: 'asc',
            width: 200
        },
        {
            label: 'Email',
            field: 'email',
            sort: 'asc',
            width: 100
        },
        {
            label: 'Status',
            field: 'status',
            sort: 'asc',
            width: 50
        },{
            label: 'Action',
            field: 'action',
            sort: 'asc',
            width: 100
        },
    ],
    rows:[]
};

class DataTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            completedata:{}
        }
    }

    componentDidMount() {
        this.setData();
    }

    componentDidUpdate = (prevProps) => {
        if (prevProps.time !== this.props.time) {
            this.setState(this.setData())
        }
    }

    setData=()=>{
        let completedata=data;
        completedata.rows=this.props.data || [];
        completedata.rows.map((_r)=>{
            _r.action= (<MDBBtn style={{backgroundColor:"blue",color:"#fff"}} size="sm" onClick={(id)=>this.editClick(_r.id)}>edit</MDBBtn>)
        });
        this.setState({completedata})
    }
    
    editClick=(id)=>{
        console.log("id",id);
    }

    render() {
        return (
            <div>
                <MDBDataTable
                    striped
                    bordered
                    small
                    data={this.state.completedata}
                />
            </div>
        )
    }
}

export default DataTable;